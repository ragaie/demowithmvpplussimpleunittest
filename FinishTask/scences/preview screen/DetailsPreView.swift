//
//  DetailsPreView.swift
//  FinishTask
//
//  Created by Ragaie Alfy on 12/2/20.
//

import UIKit
import WebKit
//previewScreenID
//cellid --> collectionCellID
class DetailsPreView: UIViewController {
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageViewItem: UIImageView!
    var model: ArticleEntity?
    override func viewDidLoad() {
        super.viewDidLoad()
        if let model = model {
            titlelabel.text = model.title ?? ""
            subTitle.text = model.abstract ?? ""
            date.text = model.publishedDate
            descriptionLabel.text = model.adxKeywords
            imageViewItem.featchImage(withUrl: model.media?[0].mediaMetadata[0].url ?? "", defaultImage: "")
        }
        // Do any additional setup after loading the view.
    }
}


