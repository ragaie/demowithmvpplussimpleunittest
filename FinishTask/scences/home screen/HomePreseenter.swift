//
//  HomePreseenter.swift
//  FinishTask
//
//  Created by Ragaie Alfy on 12/1/20.
//

import Foundation

class HomePresenter: NSObject, ViewToPresenterDelegate {
    var items: [ArticleEntity]
    weak var myView: PresenterToViewDelegate?
    var myRouter: PresenterToRouterDelegate?
    var serviceManger: ServiceManagerDelegate
    init(router:PresenterToRouterDelegate, service: ServiceManagerDelegate ) {
        myRouter = router
        serviceManger = service
        self.items = []
    }
    func opendetailScreen(item: ArticleEntity?) {
        myRouter?.opendetailScreen(model: item)
    }
    func getData() {
        let endPoint = UrlData.mostViewed(section: "all-sections", period: 7)
        serviceManger.requestDataWith(way: .GET, endPoint: endPoint, parameters: [:], classType: HomeEntity.self) { (object, data, response, error) in
            if let items = object?.results {
                self.items =  items
                self.myView?.updateItems()
            } else if let error  = error?.localizedDescription {
                self.myView?.showMessageError(message: error)
            }
        }
    }
}
