//
//  HomeView.swift
//  FinishTask
//
//  Created by Ragaie Alfy on 12/1/20.
//

import UIKit
//cell id CellID
class HomeView: UIViewController {
 
    var mypresenter: ViewToPresenterDelegate?
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var itemTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Coordinator.shared.rootNavigationController = self.navigationController
        itemTableView.delegate = self
        itemTableView.dataSource = self
        attachPresenter(presenter: HomePresenter(router: HomeRouter(),service: ServiceManager()))
        mypresenter?.getData()
        self.title = "NY Most Popular"
        navigationItem.largeTitleDisplayMode = .automatic
        self.itemTableView.accessibilityIdentifier = "TableVC_Table"

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func attachPresenter(presenter: ViewToPresenterDelegate) {
        mypresenter = presenter
        mypresenter?.myView = self
    }
}

extension HomeView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mypresenter?.items.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as? HomeTableViewCell
        cell?.setCellData(item: mypresenter?.items[indexPath.row])
        cell?.buttonClicked = {
            self.mypresenter?.opendetailScreen(item: self.mypresenter?.items[indexPath.row])
        }
        return cell ?? UITableViewCell()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if (scrollView.contentOffset.y > 0) { //20
                    self.navigationItem.largeTitleDisplayMode = .never
            } else {
                    self.navigationItem.largeTitleDisplayMode = .always
            }
        
    }
}
extension HomeView: PresenterToViewDelegate {
    func updateItems() {
        itemTableView.reloadData()
        loaderView.isHidden = true
    }
    func showMessageError(message: String) {
        loaderView.isHidden = false
        itemTableView.isHidden = true
        errorMessage.text = message
    }
}
