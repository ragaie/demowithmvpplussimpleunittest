//
//  HomeRouter.swift
//  FinishTask
//
//  Created by Ragaie Alfy on 12/1/20.
//

import Foundation
import UIKit

class HomeRouter: NSObject, PresenterToRouterDelegate {
    func opendetailScreen(model: ArticleEntity?) {
    
        let detailVc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "detailScreenID") as? DetailsPreView
        detailVc?.model = model
        Coordinator.shared.pushView(viewcontroller: detailVc ?? UIViewController())
    }
}
