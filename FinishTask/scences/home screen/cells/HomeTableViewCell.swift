//
//  HomeTableViewCell.swift
//  FinishTask
//
//  Created by Ragaie Alfy on 12/7/20.
//

import UIKit
//cell id:  cellID
class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var datelabe: UILabel!
    var buttonClicked: (()-> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCellData(item: ArticleEntity?) {
        if let model = item {
            titleLabel.text = model.title ?? ""
            subtitleLabel.text = model.abstract ?? ""
            category.text = model.byline ?? ""
            datelabe.text = model.publishedDate ?? ""
            itemImageView.featchImage(withUrl: model.media?[0].mediaMetadata[0].url ?? "", defaultImage: "")
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func cellSelected(_ sender: Any) {
        buttonClicked!()
    }
}
