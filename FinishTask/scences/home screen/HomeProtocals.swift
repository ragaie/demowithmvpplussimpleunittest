//
//  HomeProtocals.swift
//  FinishTask
//
//  Created by Ragaie Alfy on 12/1/20.
//

import Foundation

protocol ViewToPresenterDelegate: NSObjectProtocol {
    var items: [ArticleEntity] { get set }
    func opendetailScreen(item: ArticleEntity?)
    var myView: PresenterToViewDelegate? { get set }
    func getData() 
}
protocol PresenterToViewDelegate: NSObjectProtocol {
    func updateItems()
    func showMessageError(message : String)
}
protocol PresenterToRouterDelegate: NSObjectProtocol {
    func opendetailScreen(model: ArticleEntity?)
}
