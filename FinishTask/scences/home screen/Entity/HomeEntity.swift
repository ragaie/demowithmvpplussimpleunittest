//
//  HomeEntity.swift
//  FinishTask
//
//  Created by Ragaie Alfy on 12/7/20.
//

import Foundation
struct HomeEntity: Codable {
    var status: String?
    var copyright: String?
    var results: [ArticleEntity]?
}
struct ArticleEntity: Codable {
    var uri: String?
    var url: String?
    var id: Int?
    var assetId: Int?
    var source: String?
    var publishedDate: String?
    var updated: String?
    var nytdsection: String?
    var adxKeywords: String?
    var byline: String?
    var type: String?
    var title: String?
    var abstract: String?
    var desFacet: [String]?
    var media: [MediaEntity]?
    private enum CodingKeys : String, CodingKey {
           case uri,source,assetId = "asset_id", id, url, publishedDate = "published_date", updated, nytdsection, adxKeywords = "adx_keywords", byline, type, title, abstract,desFacet = "des_facet", media
       }
}

struct MediaEntity:Codable {
    var type: String?
    var subtype: String?
    var caption: String?
    var copyright: String?
    var approvedForSyndication: Int?
    var mediaMetadata: [MediaMetaDataEntity]
    private enum CodingKeys : String, CodingKey {
           case approvedForSyndication = "approved_for_syndication", mediaMetadata = "media-metadata", copyright, caption, subtype, type
       }
}
struct MediaMetaDataEntity: Codable {
    var url: String?
    var format: String?
}
