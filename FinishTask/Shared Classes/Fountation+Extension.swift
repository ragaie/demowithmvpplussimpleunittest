//
//  Fountation+Extension.swift
//  FinishTask
//
//  Created by Ragaie Alfy on 12/2/20.
//

import Foundation
import UIKit


extension UIImageView {
    func featchImage(withUrl: String, defaultImage name: String ) {
        guard let url = URL(string: withUrl) else { return }
             let task = URLSession.shared.dataTask(with: url) { data, _, _ in
                 guard let data = data else { return }
                 DispatchQueue.main.async {
                    self.image = UIImage.init(data: data) ?? UIImage.init(named: name)
             }
        }
             task.resume()
    }
}
