//
//  UrlData.swift
//  FinishTask
//
//  Created by Ragaie Alfy on 12/3/20.
//

import UIKit

class UrlData: NSObject {
    static var baseEndPoint: String! = "http://api.nytimes.com/svc/"
    static var apiKey = "b1HV4xG2x0fprEkgbAdA7joGIFxENuMo"
    static func  mostViewed(section: String, period: Int) -> String {
        return baseEndPoint + "mostpopular/v2/mostviewed/\(section)/\(period).json?api-key=\(apiKey)" }

}
