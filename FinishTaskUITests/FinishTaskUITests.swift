//
//  FinishTaskUITests.swift
//  FinishTaskUITests
//
//  Created by Ragaie Alfy on 12/1/20.
//

import XCTest

class FinishTaskUITests: XCTestCase {
    var app: XCUIApplication!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
         app = XCUIApplication()
        app.launchArguments = ["enable-testing"]
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    func testSelectFirst(){
        let tableView = app.tables.containing(.table, identifier: "TableVC_Table")
        XCTAssertTrue(tableView.cells.count > 0)
             
        let firstCell = tableView.cells.element(boundBy: 0)
        firstCell.tap()
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
